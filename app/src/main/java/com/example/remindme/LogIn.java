package com.example.remindme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.concurrent.Executors;

public class LogIn extends AppCompatActivity implements View.OnClickListener{
    Button btnLogin, btnSignup;
    String login, pass;
    EditText et_email, et_pass;
    FirebaseDatabase database;
    DatabaseReference ref;
    SharedPreferences sp;
    boolean emailFound = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        btnLogin = (android.widget.Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnSignup = (android.widget.Button) findViewById(R.id.btnSignUp);
        btnSignup.setOnClickListener(this);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("users");
        et_email = (EditText) findViewById(R.id.login_email);
        et_pass = (EditText) findViewById(R.id.login_password);

        sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        String CurrentUserID = sp.getString("UserID", "");
        if (CurrentUserID.length() > 0) {
            Intent i = new Intent(LogIn.this, MainActivity.class);
            i.putExtra("CurrentUser", CurrentUserID);
            startActivity(i);
            finish();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login = et_email.getText().toString();
                pass = et_pass.getText().toString();

                if (TextUtils.isEmpty(login)) {
                    Toast.makeText(LogIn.this, "Enter Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    Toast.makeText(LogIn.this, "Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        signIn();
                    }
                });
                //signIn(login, pass);

                break;
            case R.id.btnSignUp:
                Intent SignupScreen = new Intent(LogIn.this, SignUp.class);
                startActivity(SignupScreen);
                break;
        }
    }
    public void signIn() {
        final String login = et_email.getText().toString().toLowerCase();
        final String pass = et_pass.getText().toString().toLowerCase();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Object o = postSnapshot.getValue();
                    HashMap u = (HashMap) o;

                    Object o2 = u.get("password");
                    String password = String.valueOf(o2).toLowerCase();
                    o2 = u.get("email");
                    String email = String.valueOf(o2).toLowerCase();

                    Object loggedin = u.get("loggedin");
                    String loggedinS = String.valueOf(loggedin);

                    if (email.equals(login) && password.equals(pass)&& loggedinS.equals("false")) {
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putString("Email", email);

                        o2 = u.get("userID");
                        String uID = String.valueOf(o2);
                        ed.putString("UserID", uID);

                        DatabaseReference  databaseUsers = FirebaseDatabase.getInstance().getReference("users");

                        o2 = u.get("name");
                        String name = String.valueOf(o2);
                        ed.putString("Name", name);

                        if (ed.commit()) {
                            databaseUsers.child(uID).child("loggedin").setValue("true");
                            emailFound=true;
                            Toast.makeText(LogIn.this,"Logged in as "+ name, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LogIn.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                            Toast.makeText(getApplicationContext(), "Authenticated", Toast.LENGTH_SHORT).show();
                        }
                    } else if(email.equals(login)) {
                        emailFound=true;

                        if(!(password.equals(pass)))
                        {
                            Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Intent intent = new Intent(LogIn.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                            Toast.makeText(getApplicationContext(), "Authenticated", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                    //loadingDialog.hide();
                }
                if(emailFound==false)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(LogIn.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                        }
                    });
                ref.removeEventListener(this);
                emailFound=false;
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
