package com.example.remindme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUp extends AppCompatActivity implements View.OnClickListener{
    SharedPreferences sp;
    EditText userName, userEmail, userPassword;
    ProgressBar progressBar;
    Button signUpBtn;
    DatabaseReference databaseUsers;
    boolean flag=false;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sp= PreferenceManager.getDefaultSharedPreferences(this);
        firebaseAuth = FirebaseAuth.getInstance();
        initViews();
        userEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    String e=userEmail.getText().toString();
                    String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +"\\@" +"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +"(" +"\\." +"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +")+";
                    if(!TextUtils.isEmpty(e)) {
                        Matcher matcher = Pattern.compile(validemail).matcher(e);
                        if (!matcher.matches()) {
                            // Toast.makeText(getApplicationContext(), "Enter the correct Email", Toast.LENGTH_SHORT).show();
                            userEmail.setError("Enter the correct Email");
                        }
                    }

                }
            }
        });
    }

    void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        userName = (EditText) findViewById(R.id.UserName);
        userEmail = (EditText) findViewById(R.id.UserEmail);
        userPassword = (EditText) findViewById(R.id.UserPassword);


        signUpBtn = (Button) findViewById(R.id.SignUpIntoDatabase);
        signUpBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        progressBar.setVisibility(View.VISIBLE);
        if(v.getId() == R.id.SignUpIntoDatabase) {
            final String name = userName.getText().toString();
            final String email = userEmail.getText().toString();
            final String pass = userPassword.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(email)) {

                Toast.makeText(getApplicationContext(), "Fill all Fields", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Wait", Toast.LENGTH_LONG).show();
                databaseUsers = FirebaseDatabase.getInstance().getReference("users");
                databaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            Object o = postSnapshot.getValue();
                            HashMap u = (HashMap) o;

                            Object o2 = u.get("email");
                            String db_email = String.valueOf(o2);

                            if (db_email.equals(email)) {
                                flag=true;
                                Log.e("db-email", db_email);
                                break;
                            }
                        }
                        if(flag==true)
                        {
                            //Intent i = new Intent(SignUp.this, ExistingUserActivity.class);
                            Log.e("existing", "user");
                            Toast.makeText(getApplicationContext(), "existing: user", Toast.LENGTH_LONG).show();
                            //startActivity(i);

                        }
                        else
                        {
                            SharedPreferences.Editor ed=sp.edit();
                            ed.putString("Name",name);
                            ed.putString("Password",pass);
                            ed.putString("Email",email);
                            ed.commit();
                            // Users u=new Users(3,n,p,e,"",nic,a,nat);
                            databaseUsers.removeEventListener(this);
                            final String id = databaseUsers.push().getKey();
                            final users user = new users(id, name, email, pass);
                            databaseUsers.child(id).setValue(user);
                            Intent in = new Intent(SignUp.this, LogIn.class);
                            startActivity(in);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        }

    }


}
